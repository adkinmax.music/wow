﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vCharacterController.AI;

public class Hitboxes_Hits_Script : MonoBehaviour
{

    [FMODUnity.EventRef] public string HitEvent;
    FMOD.Studio.EventInstance HitInstance;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            HitInstance = FMODUnity.RuntimeManager.CreateInstance(HitEvent);
            HitInstance.start();
            Invoke("Release", 3f);
        }
    }

    void Release()
    {
        HitInstance.release();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
