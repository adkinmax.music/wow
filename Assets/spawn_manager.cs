using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;
using Invector.vCharacterController;

public class spawn_manager : MonoBehaviour
{
    public GameObject[] enemyPrefabs; // Создаем массив для префабов врагов
    public float rangeX = 103; // координаты для появления. Можно вводить ниже просто цифрами без переменных
    public float positionZ = 196; // позиция по оси Z
    public int numberOfenemies = 0; // в этой переменной будет храниться количество всех врагов (добавляется +1, когда враги появляются, а -1, когда враги умирают)
    public vWaypointArea waypoint; // В это поле закидываем объект WayPointArea - точки перемещения по карте AI
    int maxEnemy = 4;
    public vThirdPersonController controlHP;
    public float CurrentHealth;
    public float HPfromFMOD;


    void Start()
    {
        /*CurrentHealth = controlHP.currentHealth;*/
        
        InvokeRepeating("SpawnEnemies", 5, 10f); // Метод, который запускает функцию появления каждые 10 секунд. Перед первым появлением 5 секунд
    }

    void SpawnEnemies()
         
    {
        if (numberOfenemies <= maxEnemy)
        {
            int enemyIndex = Random.Range(0, enemyPrefabs.Length); // Выбирает случайный индекс префаба, которые мы загрузили в наш скрипт
            Vector3 spawnPosition = new Vector3(rangeX, 9, positionZ); // Случайная позиция для появления врага
            GameObject SpawnEnemyObject = Instantiate(enemyPrefabs[enemyIndex], spawnPosition, enemyPrefabs[enemyIndex].transform.rotation); // спауним префаб и подключаемся к нему в переменной SpawnEnemyObject типа GameObject (чтобы не искать через find objects)
            var EnemyController = SpawnEnemyObject.GetComponent<vControlAIMelee>(); // подключаемся к контроллеру AI
            EnemyController.onDead.AddListener(OnEnemyDead); // здесь мы следим за функцией onDead - оставляем коллбек AddListener и когда она производится мы запускаем метод OnEnemyDead
            EnemyController.waypointArea = waypoint; // Передаем заспауненному префабу точки движения
            enemyList.Add(SpawnEnemyObject); // Добавляем в список всех объектов наш новый объект префаба, чтобы за ним следить в списке
            numberOfenemies++; // Добавили в переменную, что появился новый враг, "++" это тоже самое что numberOfenemies + 1
        }
            

    }

    List<GameObject> enemyList = new List<GameObject>(); // создаем список для хнранени
    bool isAnyInCombat; // переменнтая если враг в бою

    private void Update()
    {
        bool isAnyInCombatNewState = false; // создаем промежуточную переменную для нового состояния врага
        for (int i = 0; i < enemyList.Count; i++) 
        {
            var enemymob = enemyList[i];
            var EnemyController =  enemymob.GetComponent<vControlAIMelee>();
            
            isAnyInCombatNewState = EnemyController.isInCombat;
            if (isAnyInCombatNewState) break;
        }

        if (isAnyInCombatNewState != isAnyInCombat)
        {
            isAnyInCombat = isAnyInCombatNewState;
            
            if (isAnyInCombat)
            {
                FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Battle", 1);
            }
            else FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Battle", 0); 

            
        }

        CurrentHealth = controlHP.currentHealth;
        FMODUnity.RuntimeManager.StudioSystem.getParameterByName("HEALTHNEW", out HPfromFMOD);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("HEALTHNEW", CurrentHealth);
        
    }

    void OnEnemyDead(GameObject DeadEnemy)
    {
        enemyList.Remove(DeadEnemy);
        numberOfenemies--;
    }

    private void OnGUI()
    {
        GUILayout.Space(100);
        GUILayout.Label("Game State: " +  (isAnyInCombat ? "Combat" : "Exploration" ));
        GUILayout.Label("Number of Enemies: " + numberOfenemies);
        GUILayout.Label("здоровье: " + CurrentHealth);
        GUILayout.Label("здоровье через контролХП: " + controlHP.currentHealth);
        GUILayout.Label("здоровье из ФМОД: " + HPfromFMOD);
    }
}
