﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class V_slowsteps : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string SLOWFootstepsevent;
    public vThirdPersonInput V_Input2;

    // Start is called before the first frame update
    void Start()
    {
        V_Input2 = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Footsteps()
    {
        if (V_Input2.cc.inputMagnitude > 0.1)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(SLOWFootstepsevent, gameObject);
        }

    }
}
