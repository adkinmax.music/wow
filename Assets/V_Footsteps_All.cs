﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class V_Footsteps_All : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Footsteps_event;
    public LayerMask Sloy;
    public vThirdPersonInput tpInput;
    public float CheckedSurfaceMaterial;
    //public float Run;

    


    private void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }

    private void Update()
    {
        RaycastHit LuchSolncaZolotogo;
        //Debug.Log(Run);
        if (Physics.Raycast(transform.position, Vector3.down, out LuchSolncaZolotogo, 0.3f, Sloy))
        // создаем переменную ЛучСолнцаЗолотого, запускаем рейкаст из наших ног(трансформ.позишн), вниз, 
        // сохраняем все полученные данные в созданной переменной, на дистанцию 0.3, все, что с леер маской Слой
        {
            if (LuchSolncaZolotogo.collider.CompareTag("f_DeepSnow")) CheckedSurfaceMaterial = 1;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_Wood")) CheckedSurfaceMaterial = 2;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_Water")) CheckedSurfaceMaterial = 3;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_ThinIce")) CheckedSurfaceMaterial = 4;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_Rock")) CheckedSurfaceMaterial = 5;
            //else CheckedSurfaceMaterial = 1;
        }
        
    }

    void Footsteps()
    {
        

        if (tpInput.cc.inputMagnitude > 0.5f)
        {
            //CheckSurfaceMaterial();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Footsteps_event);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            eventInstance.setParameterByName("Run", 1f);
            eventInstance.setParameterByName("Material", CheckedSurfaceMaterial);
            eventInstance.start();
            eventInstance.release();
        }
        else if (tpInput.cc.inputMagnitude > 0.1f)
        {
            //if (tpInput.cc.inputMagnitude == 0.99f)
            // CheckSurfaceMaterial();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Footsteps_event);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            eventInstance.setParameterByName("Run", 0f);
            eventInstance.setParameterByName("Material", CheckedSurfaceMaterial);
            eventInstance.start();
            eventInstance.release();
        }

    }

   

   // void CheckSurfaceMaterial()
  //  {
       // RaycastHit LuchSolncaZolotogo;
       // if (Physics.Raycast(transform.position, Vector3.down, out LuchSolncaZolotogo, 0.3f, Sloy))
     //       // создаем переменную ЛучСолнцаЗолотого, запускаем рейкаст из наших ног(трансформ.позишн), вниз, 
     //       // сохраняем все полученные данные в созданной переменной, на дистанцию 0.3, все, что с леер маской Слой
     //   {
     //       if (LuchSolncaZolotogo.collider.CompareTag("f_DeepSnow")) CheckedSurfaceMaterial = 1;
     //       else if (LuchSolncaZolotogo.collider.CompareTag("f_Wood")) CheckedSurfaceMaterial = 2;
     //       else if (LuchSolncaZolotogo.collider.CompareTag("f_Water")) CheckedSurfaceMaterial = 3;
     //       else if (LuchSolncaZolotogo.collider.CompareTag("f_ThinIce")) CheckedSurfaceMaterial = 4;
     //       else if (LuchSolncaZolotogo.collider.CompareTag("f_Rock")) CheckedSurfaceMaterial = 5;
     //       else CheckedSurfaceMaterial = 1;
     //   }
   // }

}
