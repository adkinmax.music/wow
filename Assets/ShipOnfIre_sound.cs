﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipOnfIre_sound : MonoBehaviour
{
    [FMODUnity.EventRef] public string ShipOnFire;
    public FMOD.Studio.EventInstance ShipOnFireinstance;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        ShipOnFireinstance = FMODUnity.RuntimeManager.CreateInstance(ShipOnFire);
        //ShipOnFireinstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        //or
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(ShipOnFireinstance, transform: gameObject.GetComponent<Transform>(), rigidBody: gameObject.GetComponent <Rigidbody>());
        ShipOnFireinstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
