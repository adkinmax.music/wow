﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run_steps : MonoBehaviour
{
    public AudioSource Run_steps_audiosource;
    public AudioClip[] Run_steps_audioclip;
    

    public int NewIndex;
    public int LastIndex;


    // Start is called before the first frame update
    void Start()
    {
        Run_steps_audiosource = gameObject.AddComponent<AudioSource>();
       

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Run_step()
    {
        int ClipIndex;
        Randomizator();
        Run_steps_audiosource.volume = Random.Range(0.8f, 1);
        Run_steps_audiosource.pitch = Random.Range(0.8f, 1.2f);
        Run_steps_audiosource.PlayOneShot(Run_steps_audioclip[NewIndex]);
        Debug.Log(message:"Топ");
        LastIndex = NewIndex;
    }
    
    void Randomizator()
    {
        NewIndex = Random.Range(0, Run_steps_audioclip.Length);
        while (NewIndex == LastIndex)
            NewIndex = Random.Range(0, Run_steps_audioclip.Length);
    }

}
