﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScript : MonoBehaviour
{

    public AudioSource Jump_snd_audiosource;
    public AudioClip[] Jump_snd_audioclip;
    public int NewIndex;
    public int LastIndex;

    // Start is called before the first frame update
    void Start()
    {
        Jump_snd_audiosource = gameObject.AddComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }
    void Jump_UP()
    {
        Debug.Log(message: "ХОП");
        int ClipIndex;
        Randomizator1();
        Jump_snd_audiosource.volume = Random.Range(0.8f, 1);
        Jump_snd_audiosource.pitch = Random.Range(0.8f, 1.2f);
        Jump_snd_audiosource.PlayOneShot(Jump_snd_audioclip[NewIndex]);
        
        LastIndex = NewIndex;
    }

    void Randomizator1()
    {
        NewIndex = Random.Range(0, Jump_snd_audioclip.Length);
        while (NewIndex == LastIndex)
            NewIndex = Random.Range(0, Jump_snd_audioclip.Length);
    }

}
