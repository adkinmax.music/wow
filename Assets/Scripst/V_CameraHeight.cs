﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class V_CameraHeight : MonoBehaviour
{
    public GameObject Camera;
    [FMODUnity.EventRef] public string CameraHeightWindEVENT;
    private FMOD.Studio.EventInstance CameraHeightWindINSTANCE;
    public DayNightController DayNightC;
    [FMODUnity.ParamRef] public string InsideVillageParameter;

    //public FMOD.Studio.ParameterInstance InsideID;
    //public FMOD.Studio.PARAMETER_DESCRIPTION InsideVillage;

    public GameObject DayNightG;
    // [FMODUnity.ParamRef] public string InsideVillage;
    // public FMOD.Studio.PARAMETER_ID

    // Start is called before the first frame update
    void Start()
    {
        
        CameraHeightWindINSTANCE = FMODUnity.RuntimeManager.CreateInstance(CameraHeightWindEVENT);
        CameraHeightWindINSTANCE.start();
        DayNightC = DayNightC.GetComponent<DayNightController>();
        //InsideVillage = 
    }

    // Update is called once per frame
    void Update()
    {
        CameraHeightWindINSTANCE.setParameterByName("Camera Height", Camera.transform.position.y);
        //Debug.Log(CameraHeightWindINSTANCE.getParameterByName("Camera Height", out);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("DayNight", DayNightC.currentTime);
        //Debug.Log(Camera.transform.position.y);
        //Debug.Log(DayNightC.currentTime);
    }
}
