﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vCharacterController.AI;

public class Hits_reaction : MonoBehaviour
{
    
    [FMODUnity.EventRef] public string SwooshEvent;
    FMOD.Studio.EventInstance SwooshInstance;
    [FMODUnity.EventRef] public string PunchedEvent;
    FMOD.Studio.EventInstance PunchedInstance;
    

    FMOD.Studio.EventInstance ShieldHitInstance;
    [FMODUnity.EventRef] public string ShieldHitEvent;

    FMOD.Studio.EventInstance SwordSwooshInstance;
    [FMODUnity.EventRef] public string SwordSwooshEvent;
    FMOD.Studio.EventInstance HitReactionInstance;
    [FMODUnity.EventRef] public string HitReactionEvent;



    [FMODUnity.EventRef] public string DeadEvent;
    FMOD.Studio.EventInstance DeadInstance;
    public vThirdPersonInput Hero2Input;
    public vControlAIMelee AIMelee;
    //public vThirdPersonController Hero2;
    /*[FMODUnity.EventRef] public string HitReactionEvent;
    FMOD.Studio.EventInstance HitReactionInstance;*/


    //Punched + Swoosh

    // Start is called before the first frame update
    void Start()
    {
        Hero2Input = GetComponent<vThirdPersonInput>();
        /*HitReactionInstance = FMODUnity.RuntimeManager.CreateInstance(HitReactionEvent);
        HitReactionInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        HitReactionInstance.start();
        HitReactionInstance.release();*/

    }

    void Hitreaction_small()
    {
        HitReactionInstance = FMODUnity.RuntimeManager.CreateInstance(HitReactionEvent);
        HitReactionInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        HitReactionInstance.start();
        HitReactionInstance.release();
    }

    public void PunchedByEvent()
    {
        PunchedInstance = FMODUnity.RuntimeManager.CreateInstance(PunchedEvent);
        PunchedInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        PunchedInstance.start();
        PunchedInstance.release();
        //Debug.Log("punched");
    }
    void SwordSwoosh()
    {
        SwordSwooshInstance = FMODUnity.RuntimeManager.CreateInstance(SwordSwooshEvent);
        SwordSwooshInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        SwordSwooshInstance.start();
        SwordSwooshInstance.release();
        Debug.Log("SWOOSH!");
    }
    void Swoosh()
    {
        SwooshInstance = FMODUnity.RuntimeManager.CreateInstance(SwooshEvent);
        SwooshInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        SwooshInstance.start();
        SwooshInstance.release();
        //Debug.Log("swoosh!");
    }

    public void Dead()
    {
        DeadInstance = FMODUnity.RuntimeManager.CreateInstance(DeadEvent);
        DeadInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        DeadInstance.start();
        DeadInstance.release();
    }

    private void ShieldHit()
    {
        ShieldHitInstance = FMODUnity.RuntimeManager.CreateInstance(ShieldHitEvent);
        ShieldHitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        ShieldHitInstance.start();
        ShieldHitInstance.release();
    }

/*    public void Music()
    {
        if (AIControl.currentTarget.isFixedTarget)
        {
            Debug.Log(AIControl.currentTarget);
        }
    }*/

    // Update is called once per frame
    void Update()
    {
        
    }
}
