﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX_Volume_slider_script : MonoBehaviour
{
    private UnityEngine.UI.Slider SFX_Slider;
    private FMOD.Studio.VCA SFX_VCA;
    private float SFX_Volume;
    public string VCA_Name;




    void Start()
    {
        SFX_Slider = gameObject.GetComponent<UnityEngine.UI.Slider>();
        SFX_VCA = FMODUnity.RuntimeManager.GetVCA("vca:/" + VCA_Name);
        SFX_VCA.getVolume(out SFX_Volume);
        SFX_Slider.value = SFX_Volume;
    }

    public void VCAVolumeChange()
    {

    }
    // Update is called once per frame
    void Update()
    {
        SFX_VCA.setVolume(SFX_Slider.value);
    }
}
