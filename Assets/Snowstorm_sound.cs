﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowstorm_sound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Snowstorm", gameObject.transform.position);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //Siren_on_source = gameObject.AddComponent<AudioSource>();
            FMODUnity.RuntimeManager.PlayOneShot("event:/Snowstorm", gameObject.transform.position);


        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //Siren_on_source = gameObject.AddComponent<AudioSource>();
            FMODUnity.RuntimeManager.PlayOneShot("event:/Snowstorm", gameObject.transform.position);


        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
