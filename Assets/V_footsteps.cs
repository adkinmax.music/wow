﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class V_footsteps : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Footstepsevent;
    public vThirdPersonInput V_Input;

    // Start is called before the first frame update
    void Start()
    {
        V_Input = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Runsteps()
    {
        if (V_Input.cc.inputMagnitude > 0.1)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(Footstepsevent, gameObject);
        }
        
    }
}
