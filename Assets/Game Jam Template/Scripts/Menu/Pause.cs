﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {


	private ShowPanels showPanels;						//Reference to the ShowPanels script used to hide and show UI panels
	private bool isPaused;								//Boolean to check if the game is paused or not
	private StartOptions startScript;                   //Reference to the StartButton script


	//мой кусочек
	[FMODUnity.EventRef] public string Pause_Music_event;
	FMOD.Studio.EventInstance Pause_Music_Instance;
	[FMODUnity.EventRef] public string Pause_FX_event;
	FMOD.Studio.EventInstance Pause_FX_Instance;
	[FMODUnity.EventRef] public string Whoosh_event;
	FMOD.Studio.EventInstance Whoosh_Instance;
	[FMODUnity.EventRef] public string Snap_event;
	FMOD.Studio.EventInstance Snap_Instance;

	//мой кусочек



	//Awake is called before Start()
	void Awake()
	{
		//Get a component reference to ShowPanels attached to this object, store in showPanels variable
		showPanels = GetComponent<ShowPanels> ();
		//Get a component reference to StartButton attached to this object, store in startScript variable
		startScript = GetComponent<StartOptions> ();
	}

	// Update is called once per frame
	void Update () {

		//Check if the Cancel button in Input Manager is down this frame (default is Escape key) and that game is not paused, and that we're not in main menu
		if (Input.GetButtonDown ("Escape") && !isPaused && !startScript.inMainMenu) 
		{
            Debug.Log("pausing");
			//Call the DoPause function to pause the game
			DoPause();
		} 
		//If the button is pressed and the game is paused and not in main menu
		else if (Input.GetButtonDown ("Escape") && isPaused && !startScript.inMainMenu) 
		{
			//Call the UnPause function to unpause the game
			UnPause ();
		}
	
	}


	public void DoPause()
	{
		//Set isPaused to true
		isPaused = true;
		//Set time.timescale to 0, this will cause animations and physics to stop updating
		Time.timeScale = 0;
		//call the ShowPausePanel function of the ShowPanels script
		showPanels.ShowPausePanel ();

		//мой кусочек
		Snap_Instance = FMODUnity.RuntimeManager.CreateInstance(Snap_event);
		Snap_Instance.start();
		Pause_Music_Instance = FMODUnity.RuntimeManager.CreateInstance(Pause_Music_event);
		Pause_Music_Instance.start();
		Pause_FX_Instance = FMODUnity.RuntimeManager.CreateInstance(Pause_FX_event);
		Pause_FX_Instance.start();
		Whoosh_Instance = FMODUnity.RuntimeManager.CreateInstance(Whoosh_event);
		Whoosh_Instance.start();
		Whoosh_Instance.release();
		//мой кусочек


	}


	public void UnPause()
	{
		

		//Set isPaused to false
		isPaused = false;
		//Set time.timescale to 1, this will cause animations and physics to continue updating at regular speed
		Time.timeScale = 1;
		//call the HidePausePanel function of the ShowPanels script
		showPanels.HidePausePanel ();

		//мой кусочек
		Pause_Music_Instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		Pause_Music_Instance.release();

		Pause_FX_Instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		Pause_FX_Instance.release();

		Whoosh_Instance = FMODUnity.RuntimeManager.CreateInstance(Whoosh_event);
		Whoosh_Instance.start();
		Whoosh_Instance.release();

		Snap_Instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		Snap_Instance.release();
		//мой кусочек


	}


}
