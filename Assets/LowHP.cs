﻿using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vCharacterController.AI;


public class LowHP : MonoBehaviour
{
    // Start is called before the first frame update

    [FMODUnity.EventRef] public string LowHPevent;
    FMOD.Studio.EventInstance LowHPInstance;
    public vThirdPersonController control;
    public bool SnapIsPlaying;
    public FMOD.Studio.PLAYBACK_STATE Playbackstate;

    [FMODUnity.EventRef] public string LowHP_HeartBeatevent;
    FMOD.Studio.EventInstance LowHP_HeartBeatInstance;
    public vThirdPersonController control2;
    public bool HearbeatIsPlaying;
    //public FMOD.Studio.PLAYBACK_STATE Playbackstate2;
    
    

    void Start()
    {
        LowHPInstance = FMODUnity.RuntimeManager.CreateInstance(LowHPevent);
        
    }

    private void Update()
    {
        LowHP_HeartBeatInstance.setParameterByName("Health", control.currentHealth);

        if (!HearbeatIsPlaying)
        {
            if(control.currentHealth <= 70)
            {

                LowHP_HeartBeatInstance = FMODUnity.RuntimeManager.CreateInstance(LowHP_HeartBeatevent);
                LowHP_HeartBeatInstance.start();
                HearbeatIsPlaying = true;
            }
        }
        else
        {
            if(control.currentHealth > 70)
            {
                LowHP_HeartBeatInstance.stop(STOP_MODE.ALLOWFADEOUT);
                LowHP_HeartBeatInstance.release();
                HearbeatIsPlaying = false;
            }
        }
    }

    // Update is called once per frame
    /*void Update()
    {
        //Debug.Log(control.currentHealth);

        if (!SnapIsPlaying)
        {
            if (control.currentHealth <= 40) 
            {
                
                LowHPInstance.start();
                SnapIsPlaying = true;
                Debug.Log("LOW HP!");
            }
        }
        else
        {
            if (control.currentHealth > 40)
            {
                LowHPInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                SnapIsPlaying = false;
            }
        }#1#
        
        //OR

        /*LowHPInstance.getPlaybackState(out Playbackstate);
        if (Playbackstate != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            LowHPInstance.start();
        }
        else
        {
            LowHPInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }

    }*/

    public void LowHPSnapshotStart()
    {
        LowHPInstance.getPlaybackState(out Playbackstate);
        if (Playbackstate != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            LowHPInstance.start();
            //Debug.Log(Playbackstate);
        }
    }

   /* public void LowHP_HeartBeatStart()
    {
        LowHP_HeartBeatInstance.getPlaybackState(out Playbackstate2);
        if (Playbackstate2 != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            LowHP_HeartBeatInstance.start();
            //Debug.Log(Playbackstate);
        } 
    }*/

    public void LowHPSnapshotStop()
    {
        if (control.currentHealth > 70)
        {
            LowHPInstance.getPlaybackState(out Playbackstate);
            if (Playbackstate == PLAYBACK_STATE.PLAYING)
            {
                LowHPInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
        
        
        /*if (Playbackstate == PLAYBACK_STATE.PLAYING && control.currentHealth > 40)
        {
            LowHPInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }*/
    }

    /*public void LowHP_HeartBeatStop()
    {
        if (control.currentHealth > 40)
        {
            LowHP_HeartBeatInstance.getPlaybackState(out Playbackstate2);
            if (Playbackstate2 == PLAYBACK_STATE.PLAYING)
            {
                LowHP_HeartBeatInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
    }*/

       /* void Update()
    {
        *//*if (AIControl.currentTarget.isFixedTarget)
        {
            Debug.Log(AIControl.currentTarget);
        }
           // Debug.Log(AIControl.currentTarget);*//*
        //Debug.Log(AIControl.isAiming);
    }*/
    
}
