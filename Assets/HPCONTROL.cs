﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
/*using FMODUnity;*/
using FMOD.Studio;


public class HPCONTROL : MonoBehaviour
{
    public vThirdPersonController controlHP;
    [FMODUnity.EventRef] public string MusicEvent;
    public FMOD.Studio.EventInstance MusicInstance;

    /*[EventRef] public string fmodevent;
     FMOD.Studio.EventInstance soundInstance;
     FMODUnity.StudioEventEmitter HEALTH_NEW;*/

     void Start()
     { 
        MusicInstance = FMODUnity.RuntimeManager.CreateInstance(MusicEvent);
        MusicInstance.start();

        controlHP = GetComponent<vThirdPersonController>();
    }

    private void Update()
    {

        /*Debug.Log(controlHP.currentHealth);*/
        MusicInstance.setParameterByName("HEALTH_NEW", controlHP.currentHealth);
    }


    /*void Update()
    {
        *//*FMODUnity.RuntimeManager.StudioSystem.setParameterByName("HEALTH NEW", controlHP.currentHealth);*/
        /*anEmitter.Target.SetParameter("HEALTH NEW", controlHP.currentHealth);*/
        /*Debug.Log(controlHP.currentHealth);*//*
        RuntimeManager.StudioSystem.setParameterByName("HEALTH_NEW", controlHP.currentHealth);
        
    }*/
}
