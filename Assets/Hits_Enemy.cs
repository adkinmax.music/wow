﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;
using Invector.vMelee;

public class Hits_Enemy : MonoBehaviour
{

    [FMODUnity.EventRef] public string SwooshEvent;
    FMOD.Studio.EventInstance SwooshInstance;
    [FMODUnity.EventRef] public string PunchedEvent;
    FMOD.Studio.EventInstance PunchedInstance;
    [FMODUnity.EventRef] public string DeadEvent;
    FMOD.Studio.EventInstance DeadInstance;
    public vControlAIMelee ControllerAI;
    //public vThirdPersonController Hero2;
    /*[FMODUnity.EventRef] public string HitReactionEvent;
    FMOD.Studio.EventInstance HitReactionInstance;*/


    //Punched + Swoosh

    // Start is called before the first frame update
    void Start()
    {
        ControllerAI = GetComponent<vControlAIMelee>();
        /*HitReactionInstance = FMODUnity.RuntimeManager.CreateInstance(HitReactionEvent);
        HitReactionInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        HitReactionInstance.start();
        HitReactionInstance.release();*/

    }

    public void PunchedByEvent()
    {
        PunchedInstance = FMODUnity.RuntimeManager.CreateInstance(PunchedEvent);
        PunchedInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        PunchedInstance.start();
        PunchedInstance.release();
        //Debug.Log("punched");
    }

    void Swoosh()
    {
        SwooshInstance = FMODUnity.RuntimeManager.CreateInstance(SwooshEvent);
        SwooshInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        SwooshInstance.start();
        SwooshInstance.release();
        //Debug.Log("swoosh!");
    }

    public void Dead()
    {
        DeadInstance = FMODUnity.RuntimeManager.CreateInstance(DeadEvent);
        DeadInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        DeadInstance.start();
        DeadInstance.release();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
