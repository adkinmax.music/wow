﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Roll_Sound : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Rollevent;
    public vThirdPersonInput V_Input;

    

    // Start is called before the first frame update
    void Start()
    {
        V_Input = GetComponent<vThirdPersonInput>();
    }
    
    void Rollers()
    {
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Rollevent);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        eventInstance.start();
        eventInstance.release();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
