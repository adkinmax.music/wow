﻿using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vCharacterController.AI;


public class Targeted : MonoBehaviour
{
    [FMODUnity.EventRef] public string TargetedEvent;
    FMOD.Studio.EventInstance TargetedInstance;
    public vThirdPersonController control;
    public bool IsTargeted;
    public FMOD.Studio.PLAYBACK_STATE Playbackstate;
    public vControlAIMelee AIControl;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
