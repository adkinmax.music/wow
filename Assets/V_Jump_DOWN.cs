﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class V_Jump_DOWN : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Jump_Downevent;
    public vThirdPersonInput V_Input4;

    // Start is called before the first frame update
    void Start()
    {
        V_Input4 = GetComponent<vThirdPersonInput>();
    }
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {

    }

    void Jump_DOWN()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(Jump_Downevent, gameObject);
    }
}
