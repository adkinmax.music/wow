﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class WimHoffSystem : MonoBehaviour
{
    [FMODUnity.EventRef] public string walk_run_breath;
    [FMODUnity.EventRef] public string jump_breath;
    [FMODUnity.EventRef] public string land_breath;
    [FMODUnity.EventRef] public string roll_breath;
    [FMODUnity.EventRef] public string sprint_breath;
    [FMODUnity.EventRef] public string crouch_breath;
    [FMODUnity.EventRef] public string idle_breath;
    [FMODUnity.EventRef] public string stamina_end_breath;
    [FMODUnity.EventRef] public string damage_breath;

    private vThirdPersonInput PlayerInput;

    private void Start()
    {
        PlayerInput = GetComponent<vThirdPersonInput>();
    }


    void walk_run_breath_event()
    {
        if (PlayerInput.cc.inputMagnitude > 0.1)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(walk_run_breath, gameObject);
        }
    }
    void jump_breath_event()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(jump_breath, gameObject);
    }
    void land_breath_event()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(land_breath, gameObject);
    }
    void roll_breath_event()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(roll_breath, gameObject);
    }
    void sprint_breath_event()
    {
        if (PlayerInput.cc.inputMagnitude > 1)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(sprint_breath, gameObject);
        }
        if (PlayerInput.cc.inputMagnitude < 1)
        {
            walk_run_breath_event();
        }
    }
    void crouch_breath_event()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(crouch_breath, gameObject);
    }

    void idle_breath_event()
    {
        {
            if (PlayerInput.cc.inputMagnitude < 0.1)
                FMODUnity.RuntimeManager.PlayOneShotAttached(idle_breath, gameObject);
        }
    }

    void Stamina_End()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(stamina_end_breath, gameObject);
    }

    void Damage()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(damage_breath, gameObject);
    }
}