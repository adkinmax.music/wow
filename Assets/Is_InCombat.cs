﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;
using Invector.vMelee;

public class Is_InCombat : MonoBehaviour
{

    public vControlAIMelee ControllerAI;
    public int IsInCombat;

    // Start is called before the first frame update
    void Start()
    {
        ControllerAI = GetComponent<vControlAIMelee>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(IsInCombat);
        if (ControllerAI.isInCombat == true)
        {
            IsInCombat = 1;
        }
        else
        {
            IsInCombat = 0;
        }
    }
}
