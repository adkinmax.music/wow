﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class Footsteps_Enemy : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Footsteps_event;
    public LayerMask Sloy;
    public vControlAIMelee ControllerAI;
    public float CheckedSurfaceMaterial;

    // Start is called before the first frame update
    private void Start()
    {
        ControllerAI = GetComponent<vControlAIMelee>();
    }

    private void Update()
    {
        RaycastHit LuchSolncaZolotogo2;
        //Debug.Log(Run);
        if (Physics.Raycast(transform.position, Vector3.down, out LuchSolncaZolotogo2, 0.3f, Sloy))
        // создаем переменную ЛучСолнцаЗолотого, запускаем рейкаст из наших ног(трансформ.позишн), вниз, 
        // сохраняем все полученные данные в созданной переменной, на дистанцию 0.3, все, что с леер маской Слой
        {
            if (LuchSolncaZolotogo2.collider.CompareTag("f_DeepSnow")) CheckedSurfaceMaterial = 1;
            else if (LuchSolncaZolotogo2.collider.CompareTag("f_Wood")) CheckedSurfaceMaterial = 2;
            else if (LuchSolncaZolotogo2.collider.CompareTag("f_Water")) CheckedSurfaceMaterial = 3;
            else if (LuchSolncaZolotogo2.collider.CompareTag("f_ThinIce")) CheckedSurfaceMaterial = 4;
            else if (LuchSolncaZolotogo2.collider.CompareTag("f_Rock")) CheckedSurfaceMaterial = 5;
            //else CheckedSurfaceMaterial = 1;
        }

    }

    void Footsteps()
    {


        if (ControllerAI.input.magnitude > 0.5f)
        {
            //CheckSurfaceMaterial();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Footsteps_event);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            eventInstance.setParameterByName("Run", 1f);
            eventInstance.setParameterByName("Material", CheckedSurfaceMaterial);
            eventInstance.start();
            eventInstance.release();
        }
        else if (ControllerAI.input.magnitude > 0.1f)
        {
            //if (tpInput.cc.inputMagnitude == 0.99f)
            // CheckSurfaceMaterial();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Footsteps_event);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            eventInstance.setParameterByName("Run", 0f);
            eventInstance.setParameterByName("Material", CheckedSurfaceMaterial);
            eventInstance.start();
            eventInstance.release();
        }

    }
}
