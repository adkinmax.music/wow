﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light_Red : MonoBehaviour
{
    public GameObject Lights;
    Light lt;

    // Start is called before the first frame update
    void Start()
    {
        lt.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        lt.color -= (Color.red / 10.0f) * Time.deltaTime;
    }
}
