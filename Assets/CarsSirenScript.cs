﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsSirenScript : MonoBehaviour
{
    public AudioSource Siren_on_source;
    public AudioClip Siren_on_clip;
    public AudioSource Turnoff_source;
    public AudioClip Turnoff_clip;

   
    int NewIndex;
    int LastIndex;
    int InsideCollider2;

    private void Start()
    {
        Siren_on_source = gameObject.AddComponent<AudioSource>();
        Siren_on_source.clip = Siren_on_clip;
        Siren_on_source.loop = true;
        Siren_on_source.playOnAwake = true;

        Turnoff_source = gameObject.AddComponent<AudioSource>();
        Turnoff_source.clip = Turnoff_clip;
        //Siren_on_source.loop = true;
        //Siren_on_source.playOnAwake = true;
        Siren_on_source.spatialBlend = 1;
        Siren_on_source.volume = 0f;
        Siren_on_source.PlayOneShot(Siren_on_clip);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //Siren_on_source = gameObject.AddComponent<AudioSource>();
            Siren_on_source.volume = 1f;

            SirenON();

            
        }

    }

    void SirenON()
    {

        Siren_on_source.Play();
        Turnoff_source.playOnAwake = true;
        Siren_on_source.spatialBlend = 1;
        
        
        Turnoff_source.spatialBlend = 1;
        Turnoff_source.volume = 1f;
        //Siren_on_source.PlayOneShot(Siren_on_clip);
    }
    void TurnOFF()
    {
        Turnoff_source.PlayOneShot(Turnoff_clip);
        Siren_on_source.Stop();
        Siren_on_source.loop = true;
        //Siren_on_source.volume = 0f;
        Siren_on_source.Stop();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //Invoke("TurnOff", 3);
            TurnOFF();

        }
    }
}
