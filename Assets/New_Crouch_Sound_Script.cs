﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using Invector.vCharacterController;

public class New_Crouch_Sound_Script : MonoBehaviour
{
    [FMODUnity.EventRef] public string CrouchEvent;
    FMOD.Studio.EventInstance CrouchInstance;
    public vThirdPersonController control;
    //public bool SnapIsPlaying;
    public FMOD.Studio.PLAYBACK_STATE Playbackstate;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    public void CrouchSnapshotStart()
    {
        //CrouchInstance.getPlaybackState(out Playbackstate);
        //if (Playbackstate != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        //{
        //    CrouchInstance.start();
       //     Debug.Log(Playbackstate);
       // }
        if (control.isCrouching == true)
        {
            CrouchInstance.start();
            Debug.Log(Playbackstate);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!control.isCrouching)
        {
            CrouchInstance.stop(STOP_MODE.ALLOWFADEOUT);
        }
    }
}
