﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Birps : MonoBehaviour
{

    public AudioSource Birps_audiosource;
    public AudioClip[] Birps_audioclip;
    public AudioSource BirpsFly_audiosource;
    public AudioClip[] BirpsFly_audioclip;
    private SphereCollider COLLIDER;
    
    int NewIndex;
    int LastIndex;
    int InsideCollider;

    private void Start()
    {
       
        COLLIDER = gameObject.AddComponent<SphereCollider>();
        InsideCollider = 0;
        COLLIDER.isTrigger = true;
        Birps_audiosource = gameObject.AddComponent<AudioSource>();
        Birps_audiosource.rolloffMode = AudioRolloffMode.Linear;
        Birps_audiosource.minDistance = 1;
        Birps_audiosource.maxDistance = 20;
        Birps_audiosource.spatialBlend = 1;
        
        CallAudio();
    }

    private void OnTriggerEnter(Collider other)
    {

        InsideCollider = 1;

        if (other.gameObject.tag == "Player")
        {
            // Debug.Log(message: "FlewAway");
            int ClipIndex2;
            ClipIndex2 = Random.Range(0, BirpsFly_audioclip.Length);
            BirpsFly_audiosource = gameObject.AddComponent<AudioSource>();
            BirpsFly_audiosource.clip = BirpsFly_audioclip[Random.Range(0, BirpsFly_audioclip.Length)];
            BirpsFly_audiosource.volume = Random.Range(1f, 1.2f);
            BirpsFly_audiosource.pitch = Random.Range(0.7f, 1.2f);
            BirpsFly_audiosource.spatialBlend = 1;
            BirpsFly_audiosource.PlayOneShot(BirpsFly_audioclip[ClipIndex2]);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        InsideCollider = 0;
        Invoke("CallAudio", 5);
    }
    void CallAudio()
    {
            float DelayRandom;
            DelayRandom = Random.Range(1f, 10f);
            Invoke("LeVocalisation", DelayRandom);
    }
    void LeVocalisation()
    {
        if (InsideCollider == 0)
        {
            int ClipIndex;
            ClipIndex = Random.Range(0, Birps_audioclip.Length);
            Birps_audiosource.clip = Birps_audioclip[Random.Range(0, Birps_audioclip.Length)];
            Birps_audiosource.volume = Random.Range(0.8f, 1);
            Birps_audiosource.pitch = Random.Range(0.8f, 1.2f);
            Birps_audiosource.PlayOneShot(Birps_audioclip[ClipIndex]);
        }

        if (InsideCollider == 0)
        {
            CallAudio();
        }
           
    }
   
    // Update is called once per frame
    void Update()
    {
        //Debug.Log(InsideCollider);
        //
        //if (InsideCollider == 0)
        //{
        //    Debug.Log("поют");
        //}
        //if (InsideCollider == 1)
        //{
        //    Debug.Log("НЕ поют");
        //}
    }
}
