﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTravel : MonoBehaviour
{
    private bool objectIn;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            objectIn = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            objectIn = false;
        }
    }



    void Update()
    {
        if (objectIn == true)
        {
            transform.position += transform.forward * Time.deltaTime;
        }
        
    }
}
