﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpLanding : MonoBehaviour
{

    public AudioSource Jump_landing_audiosource;
    public AudioClip[] Jump_landing_audioclip;
    public int NewIndex;
    public int LastIndex;

    // Start is called before the first frame update
    void Start()
    {
        Jump_landing_audiosource = gameObject.AddComponent<AudioSource>();
    }

    void Jump_LAND()
    {
        Debug.Log(message: "ШЛЕП");
        Randomizator1();
        Jump_landing_audiosource.volume = Random.Range(0.7f, 0.9f);
        Jump_landing_audiosource.pitch = Random.Range(0.8f, 1.1f);
        Jump_landing_audiosource.PlayOneShot(Jump_landing_audioclip[NewIndex]);

        LastIndex = NewIndex;
    }

    void Randomizator1()
    {
        NewIndex = Random.Range(0, Jump_landing_audioclip.Length);
        while (NewIndex == LastIndex)
            NewIndex = Random.Range(0, Jump_landing_audioclip.Length);
    }
}
 
