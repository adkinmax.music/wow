﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Final_crouch_script : MonoBehaviour
{
    [FMODUnity.EventRef] public string CrouchEvent;
    FMOD.Studio.EventInstance CrouchInstance;
    //public vThirdPersonInput IsCrouching;
    public vThirdPersonInput Hero2Input;
    /*public vThirdPersonController Hero2Input2;*/

    public FMOD.Studio.PLAYBACK_STATE CrouchingPlayback;


    // Start is called before the first frame update
    void Start()
    {
        //CrouchInstance = FMODUnity.RuntimeManager.CreateInstance(CrouchEvent);
    }

    /*public void Crouching_sound()
    {
        CrouchInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        CrouchInstance.start();
    }*/
    // Update is called once per frame
    void Update()
    {
        /*if (CrouchingPlayback != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            if (Hero2Input.cc.isCrouching == true)
            {
                CrouchInstance = FMODUnity.RuntimeManager.CreateInstance(CrouchEvent);
                CrouchInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
                CrouchInstance.start();
            }
            
        }
        else
        {
            CrouchInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            CrouchInstance.release();
        }*/



        //CrouchInstance.getPlaybackState(out CrouchingPlayback);
        /*CrouchInstance.getPlaybackState(out CrouchingPlayback);*/
        CrouchInstance.getPlaybackState(out CrouchingPlayback);

        if (Hero2Input.cc.isCrouching == false)
        {
            CrouchInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            CrouchInstance.release();
        }
        else if(Hero2Input.cc.isCrouching == true)
        {
            if (CrouchingPlayback != FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                CrouchInstance = FMODUnity.RuntimeManager.CreateInstance(CrouchEvent);
                CrouchInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
                CrouchInstance.start();
            }
        }
       
    }
}
