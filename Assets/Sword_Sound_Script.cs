﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vMelee;
using Invector.vCharacterController.AI;
using Invector.vCharacterController;

public class Sword_Sound_Script : MonoBehaviour
{

    [FMODUnity.EventRef] public string HitEvent;
    FMOD.Studio.EventInstance HitInstance;
    [FMODUnity.EventRef] public string RecoilEvent;
    FMOD.Studio.EventInstance RecoilInstance;
    [FMODUnity.EventRef] public string DefenceEvent;
    FMOD.Studio.EventInstance DefenceInstance;
    [FMODUnity.EventRef] public string SwordSwooshEvent;
    FMOD.Studio.EventInstance SwordSwooshInstance;
/*
    public LayerMask Material;*/
    public float CheckedHITMaterial;

    public vMeleeWeapon ControllerMelee;
    public vThirdPersonInput control;
    //public vMeleeAttackObject attackObject;
    // Start is called before the first frame update
    void Start()
    {
        /*control = GetComponent<vThirdPersonInput>();
        ControllerMelee = GetComponent<vMeleeWeapon>();*/

    }

    private void OnTriggerEnter(Collider other)
    {
        /*if (other.tag == "w_Body")
        {
            HitInstance = FMODUnity.RuntimeManager.CreateInstance(HitEvent);
            HitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            HitInstance.setParameterByName("HitMaterial", 0);
            HitInstance.start();
            HitInstance.release();
            Debug.Log("BODY-HIT-COLLIDER!");
        }*/
        if (other.tag == "Weapon")
        {
            HitInstance = FMODUnity.RuntimeManager.CreateInstance(HitEvent);
            HitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            HitInstance.setParameterByName("HitMaterial", 1);
            HitInstance.start();
            HitInstance.release();
            Debug.Log("WEAPON-HIT-COLLIDER!");
        }
       /* else if (other.tag == "w_Shield")
        {
            HitInstance = FMODUnity.RuntimeManager.CreateInstance(HitEvent);
            HitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            HitInstance.setParameterByName("HitMaterial", 2);
            HitInstance.start();
            HitInstance.release();
            Debug.Log("SHIELD-HIT-COLLIDER!");
        }*/
        /*else
        {
            HitInstance = FMODUnity.RuntimeManager.CreateInstance(HitEvent);
            HitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            HitInstance.setParameterByName("HitMaterial", 0);
            HitInstance.start();
            HitInstance.release();
            Debug.Log("ELSE-HIT-COLLIDER!");
        }*/
    }
     /*void SwordSwoosh()
    {
        SwordSwooshInstance = FMODUnity.RuntimeManager.CreateInstance(SwordSwooshEvent);
        SwordSwooshInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        SwordSwooshInstance.start();
        SwordSwooshInstance.release();
        Debug.Log("SWOOSH!");
    }*/

   /*  void PlayHitEffects()
    {
        HitInstance = FMODUnity.RuntimeManager.CreateInstance(HitEvent);
        HitInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        HitInstance.start();
        HitInstance.release();
        Debug.Log("HIT-VOID!");
    }*/

     void PlayRecoilEffects()
    {
        RecoilInstance = FMODUnity.RuntimeManager.CreateInstance(RecoilEvent);
        RecoilInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        RecoilInstance.start();
        RecoilInstance.release();
        Debug.Log("RECOIL!");
    }
     void PlayDefenseEffects()
    {
        DefenceInstance = FMODUnity.RuntimeManager.CreateInstance(DefenceEvent);
        DefenceInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        DefenceInstance.start();
        DefenceInstance.release();
        Debug.Log("DEFENCE!");
    }

    // Update is called once per frame
    void Update()
    {
        /*RaycastHit WHit;
        //Debug.Log(Run);
        if (Physics.Raycast(transform.position, Vector3.right, out WHit, 0.2f, Material))
        // создаем переменную ЛучСолнцаЗолотого, запускаем рейкаст из наших ног(трансформ.позишн), вниз, 
        // сохраняем все полученные данные в созданной переменной, на дистанцию 0.3, все, что с леер маской Слой
        {
            if (WHit.collider.CompareTag("w_Body")) CheckedHITMaterial = 0;
            else if (WHit.collider.CompareTag("w_Weapon")) CheckedHITMaterial = 1;
            else if (WHit.collider.CompareTag("w_Shield")) CheckedHITMaterial = 2;
            
            //else CheckedSurfaceMaterial = 1;
        }*/
    }
}
