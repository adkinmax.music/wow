﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Volume_slider_script : MonoBehaviour
{
    private UnityEngine.UI.Slider Music_Slider;
    private FMOD.Studio.VCA Music_VCA;
    private float Music_Volume;
    public string Music_Name;




    void Start()
    {
        Music_Slider = gameObject.GetComponent<UnityEngine.UI.Slider>();
        Music_VCA = FMODUnity.RuntimeManager.GetVCA("vca:/" + Music_Name);
        Music_VCA.getVolume(out Music_Volume);
        Music_Slider.value = Music_Volume;
    }

    public void Music_VCAVolumeChange()
    {

    }
    // Update is called once per frame
    void Update()
    {
        Music_VCA.setVolume(Music_Slider.value);
    }
}
