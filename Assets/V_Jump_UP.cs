﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class V_Jump_UP : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Jump_Downevent;
    public vThirdPersonInput V_Input4;

    public LayerMask Sloy;
    
    public float CheckedSurfaceMaterial;

    public string Jump_Upevent;
    public vThirdPersonInput V_Input3;

    // Start is called before the first frame update
    void Start()
    {
        V_Input3 = GetComponent<vThirdPersonInput>();
        V_Input4 = GetComponent<vThirdPersonInput>();
    }
    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update()
    {
        RaycastHit LuchSolncaZolotogo;
        if (Physics.Raycast(transform.position, Vector3.down, out LuchSolncaZolotogo, 0.3f, Sloy))
        // создаем переменную ЛучСолнцаЗолотого, запускаем рейкаст из наших ног(трансформ.позишн), вниз, 
        // сохраняем все полученные данные в созданной переменной, на дистанцию 0.3, все, что с леер маской Слой
        {
            if (LuchSolncaZolotogo.collider.CompareTag("f_DeepSnow")) CheckedSurfaceMaterial = 1;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_Wood")) CheckedSurfaceMaterial = 2;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_Water")) CheckedSurfaceMaterial = 3;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_ThinIce")) CheckedSurfaceMaterial = 4;
            else if (LuchSolncaZolotogo.collider.CompareTag("f_Rock")) CheckedSurfaceMaterial = 5;
            else CheckedSurfaceMaterial = 1;
        }
    }

    void Jump_UP()
    {
        //FMODUnity.RuntimeManager.PlayOneShotAttached(Jump_Upevent, gameObject);
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Jump_Upevent);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        
        eventInstance.setParameterByName("Material", CheckedSurfaceMaterial);
        eventInstance.start();
        eventInstance.release();
    }

    void Jump_DOWN()
    {
        //FMODUnity.RuntimeManager.PlayOneShotAttached(Jump_Downevent, gameObject);

        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Jump_Downevent);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));

        eventInstance.setParameterByName("Material", CheckedSurfaceMaterial);
        eventInstance.start();
        eventInstance.release();
    }
}
